CREATE TABLE users
(
  identity_id character varying NOT NULL,
  first_name character varying NOT NULL,
  last_name character varying NOT NULL,
  email character varying NOT NULL,
  fingerprint bytea   NOT NULL,
  CONSTRAINT pk_users_iden_id PRIMARY KEY (identity_id)
);

CREATE TABLE clock
(
  id serial NOT NULL,
  clock timestamp NOT NULL,
  user_id character varying NOT NULL,
  CONSTRAINT pk_clock_id PRIMARY KEY (id),
  CONSTRAINT fk_clock_user FOREIGN KEY (user_id)
      REFERENCES users (identity_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)