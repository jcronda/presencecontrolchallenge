INSERT INTO users(identity_id, first_name, last_name, email, fingerprint) VALUES ('1', 'Perico', 'Palotes', 'palotes@challenge.com', 'A');
INSERT INTO users(identity_id, first_name, last_name, email, fingerprint) VALUES ('2', 'Juan', 'Palomo', 'palomo@challenge.com', 'AB');
INSERT INTO users(identity_id, first_name, last_name, email, fingerprint) VALUES ('3', 'Jose', 'Perez', 'perez@challenge.com', 'ABC');
INSERT INTO users(identity_id, first_name, last_name, email, fingerprint) VALUES ('4', 'Javier', 'Garcia', 'Garcia@challenge.com', 'ABCD');


INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-03 08:00', 'yyyy-MM-dd hh:mm'), '1');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-03 13:00', 'yyyy-MM-dd hh:mm'), '1');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-03 14:00', 'yyyy-MM-dd hh:mm'), '1');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-03 18:00', 'yyyy-MM-dd hh:mm'), '1');

INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-03 09:00', 'yyyy-MM-dd hh:mm'), '2');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-03 18:00', 'yyyy-MM-dd hh:mm'), '2');


INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-03 08:00', 'yyyy-MM-dd hh:mm'), '3');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-03 15:00', 'yyyy-MM-dd hh:mm'), '3');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-04 08:00', 'yyyy-MM-dd hh:mm'), '3');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-04 15:00', 'yyyy-MM-dd hh:mm'), '3');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-05 08:00', 'yyyy-MM-dd hh:mm'), '3');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-05 15:00', 'yyyy-MM-dd hh:mm'), '3');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-06 08:00', 'yyyy-MM-dd hh:mm'), '3');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-06 15:00', 'yyyy-MM-dd hh:mm'), '3');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-07 08:00', 'yyyy-MM-dd hh:mm'), '3');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-07 15:00', 'yyyy-MM-dd hh:mm'), '3');

INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-03 08:00', 'yyyy-MM-dd hh:mm'), '4');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-03 15:00', 'yyyy-MM-dd hh:mm'), '4');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-04 08:00', 'yyyy-MM-dd hh:mm'), '4');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-04 15:00', 'yyyy-MM-dd hh:mm'), '4');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-05 08:00', 'yyyy-MM-dd hh:mm'), '4');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-05 09:00', 'yyyy-MM-dd hh:mm'), '4');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-06 08:00', 'yyyy-MM-dd hh:mm'), '4');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-06 15:00', 'yyyy-MM-dd hh:mm'), '4');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-07 08:00', 'yyyy-MM-dd hh:mm'), '4');
INSERT INTO public.clock(clock, user_id) VALUES (parsedatetime('2017-07-07 15:00', 'yyyy-MM-dd hh:mm'), '4');

