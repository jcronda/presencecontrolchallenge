package com.challenge.config;

import com.challenge.resources.ClockResource;
import com.challenge.resources.UsersResource;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.json.JacksonJsonProvider;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

public class JerseyInitialization extends ResourceConfig {
    /**
     * Register JAX-RS application components.
     */
    public JerseyInitialization(){
        this.register(new JacksonJsonProvider(ObjectMapperFactory.create()));

        this.property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);
        this.property(ServerProperties.BV_DISABLE_VALIDATE_ON_EXECUTABLE_OVERRIDE_CHECK, true);
        this.property(ServerProperties.TRACING, "ALL"); //can set to ON_DEMAND
        this.property(ServerProperties.TRACING_THRESHOLD, "SUMMARY"); //SUMMARY, TRACE, VERBOSE
        this.property(ServerProperties.MOXY_JSON_FEATURE_DISABLE, "true");
        this.property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, "true");
        //this.packages(true, "com.challenge.resources");

        register(UsersResource.class);
        register(ClockResource.class);
    }
}