package com.challenge.resources;

import com.challenge.persistence.ClockService;
import com.challenge.persistence.UserService;
import com.challenge.representations.Clock;
import com.challenge.representations.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.List;


@Path("/clock")
@Produces(MediaType.APPLICATION_JSON)
@Transactional
@Component
public class ClockResource {

    @Autowired
    private ClockService clockService;
    @Autowired
    private UserService userService;

    /**
     * Create new Product
     * @param fingerPrint
     * @return new product
     */
    @POST
    @Path("/save/{userId}")
    public Clock save(String fingerPrint) {
        Users user = userService.findOneByFingerprint(fingerPrint);

        Clock clock = new Clock();
        clock.setUserId(user.getIdentityId());
        clock.setClock(new Date());
        return clockService.save(clock);
    }

    /**
     * Get all clock for one user
     * @param userId
     * @return List<Clock>
     */
    @GET
    @Path("/{userId}")
    public List<Clock> getAllbyUser(@PathParam("userId")String userId) {
        return clockService.findByUser(userId);
    }

    /**
     * Get all clock for one user
     * @param userId
     * @return List<Clock>
     */
    @GET
    @Path("/{userId}/filter/{start}/{end}")
    public List<Clock> getAllbyUser(
            @PathParam("userId")String userId,
            @PathParam("start") Date start,
            @PathParam("end")   Date end) {
        return clockService.findByUser(userId, start, end);
    }
}
