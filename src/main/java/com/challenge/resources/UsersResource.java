package com.challenge.resources;


import com.challenge.Utils.DateParam;
import com.challenge.persistence.ClockService;
import com.challenge.persistence.UserService;
import com.challenge.representations.Users;
import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.util.Pair;
import org.glassfish.jersey.internal.guava.ThreadFactoryBuilder;
import org.glassfish.jersey.process.JerseyProcessingUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;


@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Component
@Transactional
public class UsersResource {

    @Autowired
    private UserService userService;
    @Autowired
    private ClockService clockService;

    /**
     * Create new user
     * @param user
     * @return new user
     */
    @POST
    public Users save(@Valid Users user) {
        return userService.save(user);
    }

    /**
     * List all users
     * @return all users
     */
    @GET
    public List<Users> getAll(){
        return userService.findAll();
    }

    /**
     * Get one user by id
     * @param userId
     * @return
     */
    @GET
    @Path("/{id}")
    public Users getOne(@PathParam("id") String userId){
        Users user = userService.findById(userId);
        if(user == null){
            throw new WebApplicationException("Users not found", Response.Status.NOT_FOUND);
        }else {
            return user;
        }
    }

    /**
     * Get time sheets user by id
     * @param userId
     * @return Map of days with minutes worked
     */
    @GET
    @Path("/time/sheets/{id}")
    public Map<Date, Long> getTimeSheet(@PathParam("id") String userId) throws ParseException {
        Map<Date, Long> map = userService.getTimeSheetById(userId);
        if(map==null){
            throw new WebApplicationException("Users not found", Response.Status.NOT_FOUND);
        }else {
            return map;
        }
    }

    /**
     * Get absence time by id
     * @param userId
     * @param start
     * @param end
     * @return Minutes absense
     */
    @GET
    @Path("/time/absence/{id}/{start}/{end}")
    public Long getAbsenceTime(
            @PathParam("id") String userId,
            @PathParam("start") DateParam start,
            @PathParam("end") DateParam end) {
        Users user = userService.findById(userId);
        if(user == null){
            throw new WebApplicationException("Users not found", Response.Status.NOT_FOUND);
        }else {
            return userService.getAbsenceTimeById(userId, start.getDate(), end.getDate());
        }
    }

    /**
     * Get absence time by id
     * @param start
     * @param end
     * @return Minutes absense
     */
    @GET
    @Path("/time/absence/{start}/{end}")
    public void getAbsenceTimeAllUserAsync(
            @PathParam("start") DateParam start,
            @PathParam("end") DateParam end,
            final @Suspended AsyncResponse asyncResponse) throws InterruptedException, ExecutionException {
        //Como puede ser un proceso costoso, lo quiero ejecutar en multihilo. Para ello uso Callable y Future
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        //Obtengo la lista de usuarios
        List<Users> usersList = userService.findAll();

        if(usersList == null){
            throw new WebApplicationException("Users not found", Response.Status.NOT_FOUND);
        }else {
            Map<String, Long> responseMap = new HashMap<>();
            //

            List<Callable<Pair>> callableList = usersList.stream()
                    .map(u -> getCallableOfAbsenceTime(u.getIdentityId(), start.getDate(), end.getDate()))
                    .collect(Collectors.toList());

            List<Future<Pair>> futureList = executorService.invokeAll(callableList);

            for (Future<Pair> future: futureList) {
                Pair<String, Long> pair = future.get();
                responseMap.put(pair.getKey(), pair.getValue());
            }

            asyncResponse.resume(responseMap);
        }
    }

    //Defino un método de callable para poder pasarle parametros.
    private Callable<Pair> getCallableOfAbsenceTime(String user, Date start, Date end) {
        final String userf = user;
        final Date startf = start;
        final Date endf = end;

        return () -> {
            Long absence = userService.getAbsenceTimeById(userf, startf, endf);
            return  new Pair(user, absence);
        };
    }
}