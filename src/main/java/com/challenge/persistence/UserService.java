package com.challenge.persistence;

import com.challenge.representations.Users;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public interface UserService {

    @Transactional
    Users save(Users user);

    List<Users> findAll();

    Users findOneByFingerprint(String fingerPrint);

    Users findById(String userId);

    Map<Date,Long> getTimeSheetById(String userId) throws ParseException;

    Long getAbsenceTimeById(String userId, Date start, Date end);
}
