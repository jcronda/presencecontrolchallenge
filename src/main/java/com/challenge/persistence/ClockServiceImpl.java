package com.challenge.persistence;

import com.challenge.representations.Clock;
import com.challenge.representations.Users;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.Date;
import java.util.List;

@Service("clockService")
@Transactional(readOnly = true)
public class ClockServiceImpl implements ClockService{

    @PersistenceContext
    private EntityManager em;

    @Autowired
    ClockRepository repository;

    @Override
    @Transactional
    public Clock save(Clock clock) {
        return repository.save(clock);
    }

    @Override
    public List<Clock> findByUser(String userId) {

        Query query = em.createQuery("SELECT c "
                +" FROM Users as u LEFT JOIN u.clock as c" +
                " where u.identityId = :identityId ")
                .setParameter("identityId", userId);

        return query.getResultList();
    }

    @Override
    public List<Clock> findByUser(String userId, Date start, Date end) {

        Query query = em.createQuery("SELECT c "
                +" FROM Users as u LEFT JOIN u.clock as c" +
                " where u.identityId = :identityId " +
                " and c.clock BETWEEN :start and :end ")
                .setParameter("identityId", userId)
                .setParameter("start", start)
                .setParameter("end", end);

        return query.getResultList();
    }
}