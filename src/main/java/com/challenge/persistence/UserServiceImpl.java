package com.challenge.persistence;

import com.challenge.Utils.WorkingTime;
import com.challenge.representations.Clock;
import com.challenge.representations.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


@Service("userService")
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService{

    @PersistenceContext
    private EntityManager em;

    @Autowired
    UserRepository repository;

    @Autowired
    private ClockService clockService;

    @Override
    @Transactional
    public Users save(Users user) {
        return repository.save(user);
    }

    @Override
    public List<Users> findAll() {
        return repository.findAll();
    }

    @Override
    public Users findOneByFingerprint(String fingerPrint) {
        List<Users> usersList =
                em.createQuery("Select u from Users u where fingerPrint = :fingerPrint")
                        .setParameter("fingerPrint", fingerPrint)
                .getResultList();
        return usersList.isEmpty()? null : usersList.get(0);
    }

    @Override
    @Transactional
    public Users findById(String userId) {
        Optional<Users> userOptional = repository.findById(userId);
        return userOptional.isPresent() ? userOptional.get() : null;
    }

    @Override
    public Map<Date, Long> getTimeSheetById(String userId) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Optional<Users> userOptional = repository.findById(userId);
        Map<Date, Long> map = new TreeMap<Date,Long>();

        if (userOptional.isPresent() && !userOptional.get().getClock().isEmpty()) {
            Map<String, List<Clock>> collect = userOptional.get().getClock().stream().collect(Collectors.groupingBy(c -> df.format(c.getClock())));

            for (Map.Entry<String, List<Clock>> entry : collect.entrySet()) {
                Long minutes = 0L;
                //The clock's list is ordered
                List<Clock> values = entry.getValue();
                for (int i = 0; i < values.size(); i=i+2){
                    try{
                        long diff = values.get(i+1).getClock().getTime() - values.get(i).getClock().getTime();
                        minutes += diff / (60 * 1000);
                    } catch (Exception e) {
                        //Numero Impar de horas.
                    }
                }
                map.put(df.parse(entry.getKey()), minutes);
            }
        }
        return map;
    }

    @Override
    public Long getAbsenceTimeById(String userId, Date start, Date end) {
        WorkingTime workingTime = new WorkingTime(8, 15);
        Long exectWorkingTime = workingTime.calculateWorkingMinutes(start, end);
        Long realWorkingTime = 0L;

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        List<Clock> userList = clockService.findByUser(userId, start, end);

        if (!userList.isEmpty()) {
            Map<String, List<Clock>> collect = userList.stream().collect(Collectors.groupingBy(c -> df.format(c.getClock())));

            for (Map.Entry<String, List<Clock>> entry : collect.entrySet()) {
                List<Clock> values = entry.getValue();
                for (int i = 0; i < values.size(); i=i+2){
                    try{
                        long diff = values.get(i+1).getClock().getTime() - values.get(i).getClock().getTime();
                        realWorkingTime += diff / (60 * 1000);
                    } catch (Exception e) {
                        //Numero Impar de horas.
                    }
                }
            }
        }
        return exectWorkingTime - realWorkingTime;
    }

}