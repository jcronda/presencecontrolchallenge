package com.challenge.persistence;

import com.challenge.representations.Clock;
import com.challenge.representations.Users;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public interface ClockService {

    Clock save(Clock clock);

    List<Clock> findByUser(String user);

    List<Clock> findByUser(String userId, Date start, Date end);


}
