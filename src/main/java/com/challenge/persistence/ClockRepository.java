package com.challenge.persistence;

import com.challenge.representations.Clock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClockRepository extends JpaRepository<Clock, Long> {

}