package com.challenge;

import com.challenge.persistence.ClockService;
import com.challenge.persistence.UserService;
import com.challenge.representations.Clock;
import com.challenge.representations.Users;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ClockServiceTest {

	@Autowired
    ClockService clockService;

    @Test
    public void testSave() throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    	Clock c = new Clock();
    	c.setClock(df.parse("2018-01-01"));
    	c.setUserId("1");

    	Clock saved = clockService.save(c);

    	Assert.assertNotNull(saved);
    	Assert.assertEquals("1", saved.getUserId());
    	Assert.assertEquals("2018-01-01", df.format(saved.getClock()));
    }

    @Test
    public void testfindByUser() {

		List<Clock> clockList = clockService.findByUser("2");

		Assert.assertNotNull(clockList);
		Assert.assertEquals(2, clockList.size());

    }

    @Test
    public void testfindByUserFilterDate() throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		List<Clock> clockList = clockService.findByUser("3", df.parse("2017-07-01"), df.parse("2017-07-05"));

		Assert.assertNotNull(clockList);
		Assert.assertEquals(4, clockList.size());

    }


}