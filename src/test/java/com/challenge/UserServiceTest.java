package com.challenge;

import com.challenge.persistence.UserService;
import com.challenge.representations.Users;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserServiceTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Autowired
    UserService userService;

    @Test
    public void testSave() {

		Users u = new Users("00000", "Maria", "Perez", "Perez@challenge.com", "ABtsdCD" );

		Users saved = userService.save(u);

		Assert.assertNotNull(saved);
		Assert.assertEquals("00000", saved.getIdentityId());
		Assert.assertEquals("Maria", saved.getFirstName());
		Assert.assertEquals("Perez", saved.getLastName());
		Assert.assertEquals("Perez@challenge.com", saved.getEmail());
		Assert.assertEquals("ABtsdCD", saved.getFingerPrint());

		List<Users> usersList = userService.findAll();

		Assert.assertNotNull(usersList);
		Assert.assertEquals(5, usersList.size());
    }

    @Test
    public void testFindAll() {

    	List<Users> usersList = userService.findAll();

    	Assert.assertNotNull(usersList);
    	Assert.assertEquals(4, usersList.size());
    }

    @Test
    public void testFindByFingerprint() {

		Users notUser = userService.findOneByFingerprint("CJS");
		Assert.assertNull(notUser);

		Users user = userService.findOneByFingerprint("AB");

		Assert.assertNotNull(user);
		Assert.assertEquals("2", user.getIdentityId());
		Assert.assertEquals("Juan", user.getFirstName());
		Assert.assertEquals("palomo@challenge.com", user.getEmail());
    }

    @Test
    public void testFindById() {

		Users notUser = userService.findById("0");
		Assert.assertNull(notUser);

    	Users user = userService.findById("1");

    	Assert.assertNotNull(user);
    	Assert.assertEquals("1", user.getIdentityId());
    	Assert.assertEquals("Perico", user.getFirstName());
    	Assert.assertEquals("palotes@challenge.com", user.getEmail());
    }

	@Test
	public void testTimeSheetById() throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Map<Date, Long> timeSheetNull = userService.getTimeSheetById("0");
		Assert.assertTrue(timeSheetNull.isEmpty());

		Map<Date, Long> timeSheet = userService.getTimeSheetById("1");

		Assert.assertNotNull(timeSheet);
		Assert.assertEquals(1, timeSheet.size());
		Assert.assertEquals(540L, timeSheet.values().stream().reduce(0L, Long::sum).longValue());

	}

	@Test
	public void testAbsenceTimeById() throws ParseException {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		Long absenceTimeTotal = userService.getAbsenceTimeById("0", df.parse("2017-07-03"), df.parse("2017-07-09"));
		Assert.assertEquals(Long.valueOf(2100), absenceTimeTotal);

		//Falto 7 horas
		Long absenceTime = userService.getAbsenceTimeById("4", df.parse("2017-07-03"), df.parse("2017-07-09"));
		Assert.assertEquals(Long.valueOf(360), absenceTime);

	}

	@Test
	public void testAllAbsenceTimeById() throws ParseException {
		Map<String, Integer> response = this.restTemplate.getForObject("/user/time/absence/2017-07-01/2017-07-09", Map.class);
		Assert.assertNotNull(response);
		Assert.assertEquals(Long.valueOf(response.get("4")), Long.valueOf(360));


	}


}